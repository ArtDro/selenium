from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ArenaHomeworkPage:

    def __init__(self, browser):
        self.browser = browser

    def wait_for_text_area_load(self):
        wait = WebDriverWait(self.browser, 10)
        text_area = (By.CSS_SELECTOR, '#j_msgContent')
        wait.until(expected_conditions.element_to_be_clickable(text_area))

    def add_project(self):
        self.browser.find_element(By.CSS_SELECTOR, 'a.button_link').click()

    def insert_message(self, random_text):
        self.browser.find_element(By.CSS_SELECTOR, '#j_msgContent').send_keys(random_text)

    def insert_name(self, random_text):
        self.browser.find_element(By.CSS_SELECTOR, 'input#name').send_keys(random_text)

    def insert_prefix(self, random_text):
        self.browser.find_element(By.CSS_SELECTOR, 'input#prefix').send_keys(random_text)

    def insert_textarea(self, random_text):
        self.browser.find_element(By.CSS_SELECTOR, 'textarea#description').send_keys(random_text)

    def save_project(self):
        self.browser.find_element(By.CSS_SELECTOR, 'input#save').click()

    def activated_menu(self):
        self.browser.find_element(By.CSS_SELECTOR, 'a.activeMenu').click()

    def result_searching(self):
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()



from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class SearchResultPage:
    def __init__(self, browser):
        self.browser = browser

    def verify_post_count(self, expected_count):
        titles = self.browser.find_elements(By.CSS_SELECTOR, '.post-title')
        assert len(titles) == expected_count



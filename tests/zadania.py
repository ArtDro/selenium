import time

from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager


def test_should_open_duckduckgo_selenium_test():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('http://duckduckgo.com')
    browser.set_window_size(1024, 768)
    assert 'DuckDuckGo' in browser.title
    time.sleep(3)
    browser.quit()

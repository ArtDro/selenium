import pytest
import time
from selenium.webdriver import Chrome, Keys
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC, expected_conditions


@pytest.fixture()
def browser():
    # 1 - to wykona się PRZED każdym testem który korzysta z tego fixture'a
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('https://awesome-testing.blogspot.com/')
    browser.set_window_size(1920, 1080)
    # 2 -  to jest zmienna przekazana do testów
    yield browser
    # 3 -  to wykona się PO każdym teście który korzysta z tego fixture'1
    browser.quit()


def test_post_count(browser):
    browser.find_element(By.ID, 'cookieChoiceDismiss').click()

    search_input = browser.find_element(By.CSS_SELECTOR, 'input.gsc-input')
    search_button = browser.find_element(By.CSS_SELECTOR, '.gsc-search-button')

    search_input.send_keys('selenium')
    search_button.click()

    # Pobranie listy tytułów/postów
    list_titles = browser.find_elements(By.CSS_SELECTOR, 'h1')

    # Asercja
    assert len(list_titles) == 20


def test_post_count_after_search(browser):
    search_input = browser.find_element(By.CSS_SELECTOR, 'input.gsc-input')
    search_button = browser.find_element(By.CSS_SELECTOR, '.gsc-search-button')

    # Szukanie
    search_input.send_keys('selenium')
    search_button.click()

    # Pobranie listy tytułów
    list_titles = browser.find_elements(By.CSS_SELECTOR, 'h1')

    # Asercja że lista ma 3 elementy
    assert len(list_titles) == 20


def test_post_count_on_2016_label(browser):
    # Inicjalizacja elementu z labelką
    search_input = browser.find_element(By.LINK_TEXT, '2016')

    # Kliknięcie na labelkę 2016
    search_input.click()

    # Pobranie listy tytułów
    list_titles = browser.find_elements(By.CSS_SELECTOR, 'h1')

    # Asercja że lista ma 24 elementy
    assert len(list_titles) == 24

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from pages.arena.arena_home_page import ArenaHomePage
from pages.arena.arena_homework import ArenaHomeworkPage
from pages.arena.arena_login import ArenaLoginPage
from pages.arena.arena_project_page import ArenaProjectPage
from utilis.random_message import generate_random_text


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = ArenaLoginPage(browser)
    login_page.load()
    login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield browser
    browser.quit()


def test_should_open_projects_page(browser):
    project_page = ArenaHomePage(browser)
    project_page.click_tools_icon()
    arena_project_page = ArenaProjectPage(browser)
    arena_project_page.verify_title('Projekty')

    arena_homework_page = ArenaHomeworkPage(browser)
    arena_homework_page.add_project()
    
    random_text_name = generate_random_text(4)
    arena_homework_page = ArenaHomeworkPage(browser)
    arena_homework_page.insert_name(random_text_name)

    random_text = generate_random_text(10)
    arena_homework_page = ArenaHomeworkPage(browser)
    arena_homework_page.insert_prefix(random_text)

    random_text = generate_random_text(125)
    arena_homework_page = ArenaHomeworkPage(browser)
    arena_homework_page.insert_textarea(random_text)

    arena_homework_page = ArenaHomeworkPage(browser)
    arena_homework_page.save_project()

    arena_homework_page = ArenaHomeworkPage(browser)
    arena_homework_page.activated_menu()

    browser.find_element(By.CSS_SELECTOR, 'input#search').send_keys(random_text_name)

    arena_homework_page = ArenaHomeworkPage(browser)
    arena_homework_page.result_searching()

    browser.quit()

import time

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_baidu():
    browser = Chrome(executable_path=ChromeDriverManager().install())

    browser.get('https://www.baidu.com/')
    browser.set_window_size(1920, 1080)

    search_input = browser.find_element(By.ID, 'kw')
    search_input.send_keys('4_Testers')
    search_input.click()

    time.sleep(10)

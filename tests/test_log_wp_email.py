import time

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from tests import credentials


def test_log_to_wp_email():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('http://www.poczta.wp.pl')
    browser.set_window_size(1920, 1080)
    browser.find_element(By.CSS_SELECTOR, 'div > button:nth-child(2)').click()

    username = credentials.CLIPPINGS_IO_USERNAME
    password = credentials.CLIPPINGS_IO_PASSWORD

    browser.find_element(By.CSS_SELECTOR, '#login').send_keys(username)
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)
    time.sleep(1)
    browser.find_element(By.CSS_SELECTOR, 'div > form > button').click()
    time.sleep(3)
    browser.quit()

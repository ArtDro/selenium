import time

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

from pages.arena.arena_messages import ArenaMessagesPage
from utilis.random_message import generate_random_text


@pytest.fixture
def browser():
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    driver.set_window_size(1920, 1080)
    driver.find_element(By.ID, 'email').send_keys('administrator@testarena.pl')
    driver.find_element(By.ID, 'password').send_keys('sumXQQ72$L')
    driver.find_element(By.ID, 'login').click()
    driver.find_element(By.CSS_SELECTOR, '.icon_mail').click()
    wait = WebDriverWait(driver, 10)
    wait.until(expected_conditions.element_to_be_clickable((By.ID, 'j_msgContent')))
    yield driver
    driver.quit()


def test_should_add_new_message(browser):
    random_text = generate_random_text(10)
    arena_messages_page = ArenaMessagesPage(browser)
    arena_messages_page.wait_for_text_area_load()
    arena_messages_page.insert_message(random_text)
    time.sleep(5)

    
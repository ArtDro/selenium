import pytest
import time
from selenium.webdriver import Chrome, Keys
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC, expected_conditions

from pages.home import HomePage
from pages.search_result import SearchResultPage


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('https://awesome-testing.blogspot.com/')
    browser.set_window_size(1920, 1080)
    yield browser
    browser.quit()


def test_post_count(browser):
    home_page = HomePage(browser)
    home_page.verify_post_count(4)


def test_post_count_after_search(browser):
    home_page = HomePage(browser)
    home_page.search_for('selenium')

    search_result_page = SearchResultPage(browser)
    search_result_page.verify_post_count(20)


def test_post_count_on_2016_label(browser):
    home_page = HomePage(browser)
    home_page.click_label('2016')
    search_result_page = SearchResultPage(browser)
    search_result_page.verify_post_count(24)



